import React from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

// Material
import {
  Divider,
  Drawer,
  IconButton,
  Container,
  makeStyles,
  Toolbar
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/CloseRounded";

// Components
import CharacterDetail from "../CharacterDetail";
import MovieDetail from "../MovieDetail";

// Config
import { drawerWidth } from "../../config/config";

const useStyles = makeStyles(theme => ({
  paper: {
    width: "100vw",
    [theme.breakpoints.up("sm")]: {
      width: `calc((100vw - ${drawerWidth}px) / 2)`
    }
  }
}));

const detail = {
  characters: CharacterDetail,
  movies: MovieDetail
};

const DetailDrawer = ({ onClose, data, type, id }) => {
  const classes = useStyles();
  const Detail = detail[type];
  const { isOpen } = useSelector(state => state.detail);

  return (
    <Drawer variant="persistent" anchor="right" open={isOpen} classes={classes}>
      <Toolbar>
        <IconButton onClick={onClose()}>
          <CloseIcon />
        </IconButton>
      </Toolbar>
      <Divider />
      <Container>
        <Detail data={data} id={id} />
      </Container>
    </Drawer>
  );
};

DetailDrawer.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string
  }).isRequired,
  onClose: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  id: PropTypes.string
};

DetailDrawer.defaultProps = {
  id: ""
};

export default DetailDrawer;
