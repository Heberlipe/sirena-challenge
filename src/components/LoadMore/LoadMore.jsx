import React from "react";
import PropTypes from "prop-types";
import {
  Button,
  CircularProgress,
  Container,
  makeStyles
} from "@material-ui/core";
import { useBottomScrollListener } from "react-bottom-scroll-listener";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    justifyContent: "center",
    padding: theme.spacing(2, 0)
  }
}));

const LoadMore = ({ onClick, isLoading }) => {
  const classes = useStyles();
  useBottomScrollListener(onClick);
  return (
    <Container className={classes.root}>
      {isLoading ? (
        <CircularProgress />
      ) : (
        <Button variant="outlined" color="primary" onClick={onClick}>
          Cargar más
        </Button>
      )}
    </Container>
  );
};

LoadMore.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};

export default LoadMore;
