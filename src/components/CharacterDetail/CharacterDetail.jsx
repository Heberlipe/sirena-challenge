import React from "react";
import { useSelector } from "react-redux";
import { List, ListItem, Typography } from "@material-ui/core";
import PropTypes from "prop-types";

import DataItem from "../DataItem";
import DetailStatusHandler from "../DetailStatusHandler";
import MovieLink from "../MovieLink";
import { useTranslate } from "../../hooks";

const CharacterDetail = ({ data }) => {
  const { content, isLoading, isEmpty } = useTranslate(data, "eye_color");
  const { data: movies, isLoading: isLoadingMovies } = useSelector(
    state => state.movies
  );

  return isLoading || isLoadingMovies || isEmpty ? (
    <DetailStatusHandler isLoading={isLoading} />
  ) : (
    <List>
      <DataItem isTitle value={content.name} />
      <DataItem label="Color de ojos" value={content.eye_color} />
      <DataItem label="Altura" value={content.height} unit="cm" />
      <DataItem label="Peso" value={content.mass} unit="kg" />
      <List>
        <ListItem>
          <Typography variant="subtitle2">
            Peliculas en las que aparece:
          </Typography>
        </ListItem>
        {content.films.map(id => (
          <MovieLink
            key={id}
            to={`/peliculas/${id}`}
            title={movies.find(movie => movie.id === id).title}
          />
        ))}
      </List>
    </List>
  );
};

CharacterDetail.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    height: PropTypes.string,
    eye_color: PropTypes.string,
    mass: PropTypes.string,
    films: PropTypes.arrayOf(PropTypes.string)
  })
};

CharacterDetail.defaultProps = {
  data: {}
};

export default CharacterDetail;
