import React from "react";
import PropTypes from "prop-types";
import { ListItem, makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  label: {
    margin: theme.spacing(0, 1, 0, 0)
  }
}));

const DataItem = ({ label, value, isTitle, unit }) => {
  const classes = useStyles();
  return (
    <ListItem>
      {label && (
        <Typography className={classes.label} variant="subtitle2">
          {label}:
        </Typography>
      )}
      <Typography variant={isTitle ? "h6" : "body2"}>
        {value === "unknown" ? "desconocido" : `${value} ${unit}`}
      </Typography>
    </ListItem>
  );
};

DataItem.propTypes = {
  isTitle: PropTypes.bool,
  label: PropTypes.string,
  unit: PropTypes.string,
  value: PropTypes.string.isRequired
};

DataItem.defaultProps = {
  isTitle: false,
  unit: "",
  label: null
};

export default DataItem;
