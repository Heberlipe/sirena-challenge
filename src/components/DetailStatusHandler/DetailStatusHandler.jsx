import React from "react";
import PropTypes from "prop-types";
import { CircularProgress, makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  center: {
    display: "flex",
    justifyContent: "center",
    width: "100%",
    padding: theme.spacing(2)
  }
}));

const DetailStatusHandler = ({ isLoading }) => {
  const classes = useStyles();

  return (
    <div className={classes.center}>
      {isLoading ? (
        <CircularProgress />
      ) : (
        <Typography variant="caption">
          No hay ningun elemento seleccionado
        </Typography>
      )}
    </div>
  );
};

DetailStatusHandler.propTypes = {
  isLoading: PropTypes.bool.isRequired
};

export default DetailStatusHandler;
