import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { Typography, ListItem } from "@material-ui/core";

const MovieLink = ({ title, to }) => (
  <ListItem button component={Link} to={to}>
    <Typography variant="body2">
      <i>{title}</i>
    </Typography>
  </ListItem>
);

MovieLink.propTypes = {
  title: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired
};

export default MovieLink;
