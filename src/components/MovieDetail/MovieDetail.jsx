import React from "react";
import { isEmpty } from "lodash/fp";
import { List } from "@material-ui/core";
import PropTypes from "prop-types";

import DataItem from "../DataItem";
import DetailStatusHandler from "../DetailStatusHandler";

const MovieDetail = ({ data }) =>
  isEmpty(data) ? (
    <DetailStatusHandler isLoading={false} />
  ) : (
    <List>
      <DataItem isTitle value={data.title} />
      <DataItem label="Director" value={data.director} />
      <DataItem label="Productor" value={data.producer} />
      <DataItem label="Fecha de estreno" value={data.release_date} />
    </List>
  );

MovieDetail.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    director: PropTypes.string,
    producer: PropTypes.string,
    release_date: PropTypes.string
  })
};

MovieDetail.defaultProps = {
  data: {}
};

export default MovieDetail;
