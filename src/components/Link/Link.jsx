import React from "react";
import { Link as RouterLink } from "react-router-dom";

const Link = React.forwardRef((props, ref) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <RouterLink innerRef={ref} {...props} />
));

export default Link;
