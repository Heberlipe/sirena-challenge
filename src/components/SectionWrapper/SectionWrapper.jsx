import React from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import { Container, makeStyles } from "@material-ui/core";
import { customPropTypes } from "../../utils";

const useStyles = makeStyles(theme => ({
  container: {
    margin: theme.spacing(2, 0)
  }
}));

const SectionWrapper = ({ children, className }) => {
  const classes = useStyles();
  return (
    <Container
      component="section"
      className={clsx(classes.container, className)}
    >
      {children}
    </Container>
  );
};

SectionWrapper.propTypes = {
  children: customPropTypes.children,
  className: PropTypes.string
};

SectionWrapper.defaultProps = {
  children: null,
  className: ""
};

export default SectionWrapper;
