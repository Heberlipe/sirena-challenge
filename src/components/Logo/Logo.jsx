import React from "react";
import PropTypes from "prop-types";
import logo from "./logo.png";

const Logo = ({ height }) => <img src={logo} alt="logo" height={height} />;

Logo.propTypes = {
  height: PropTypes.string
};

Logo.defaultProps = {
  height: "50px"
};

export default Logo;
