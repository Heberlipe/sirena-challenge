import React from "react";
import PropTypes from "prop-types";
import { List, ListItem, makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(0)
  },
  text: {
    padding: theme.spacing(2)
  }
}));

const ContentList = ({ data, onClick, selected, isLoading }) => {
  const classes = useStyles();

  return (
    <List className={classes.root}>
      {!isLoading && data.length === 0 && (
        <Typography className={classes.text} variant="h6" align="center">
          Lo sentimos, no encontramos lo que estas buscando.
        </Typography>
      )}
      {data.map(({ id, name, title }) => (
        <ListItem
          key={id}
          button
          selected={id === selected}
          onClick={onClick(id)}
        >
          <Typography variant="h6">{name || title}</Typography>
        </ListItem>
      ))}
    </List>
  );
};

ContentList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string
    })
  ).isRequired,
  onClick: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  selected: PropTypes.string.isRequired
};

export default ContentList;
