import React from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { grey } from "@material-ui/core/colors";
import clsx from "clsx";

// Material
import MenuIcon from "@material-ui/icons/Menu";
import {
  AppBar,
  IconButton,
  Toolbar,
  Typography,
  makeStyles
} from "@material-ui/core";

// Components
import { DrawerContainer, DrawerWrapper, Search } from "./components";

// Config
import { drawerWidth } from "../../config/config";
import { getNavProps } from "../../config/routes";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  appBar: {
    backgroundColor: grey[800],
    color: "#fff",
    marginLeft: drawerWidth,
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`
    }
  },
  toolbar: {
    width: "100%",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  toolbarShift: {
    [theme.breakpoints.up("sm")]: {
      width: " 50%"
    }
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  },
  title: {
    flexGrow: 1,
    display: "block",
    [theme.breakpoints.down("xs")]: {
      display: ({ showSearch }) => (showSearch ? "none" : "block")
    }
  }
}));

const NavBar = ({ location: { pathname } }) => {
  const { title, showSearch } = getNavProps(pathname);
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const { isOpen } = useSelector(state => state.detail);
  const classes = useStyles({ showSearch });

  const toggleDrawer = value => event => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setMobileOpen(value);
  };

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar
          className={clsx(classes.toolbar, {
            [classes.toolbarShift]: isOpen
          })}
        >
          <IconButton
            color="inherit"
            edge="start"
            onClick={toggleDrawer(true)}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap className={classes.title}>
            {title}
          </Typography>
          {showSearch && <Search />}
        </Toolbar>
      </AppBar>
      <DrawerContainer mobileOpen={mobileOpen} toggleDrawer={toggleDrawer}>
        <DrawerWrapper onClose={toggleDrawer(false)} />
      </DrawerContainer>
    </div>
  );
};

NavBar.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired
};

export default withRouter(NavBar);
