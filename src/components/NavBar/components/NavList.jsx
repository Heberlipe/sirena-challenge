import React from "react";
import PropTypes from "prop-types";
import { List, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import Link from "../../Link";

const NavList = ({ content, onClick }) => (
  <List>
    {content.map(({ Icon, text, to }) => (
      <ListItem button onClick={onClick} component={Link} to={to} key={to}>
        {Icon && (
          <ListItemIcon>
            <Icon />
          </ListItemIcon>
        )}
        <ListItemText primary={text} />
      </ListItem>
    ))}
  </List>
);

NavList.propTypes = {
  content: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onClick: PropTypes.func.isRequired
};

export default NavList;
