import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

// Material
import SearchIcon from "@material-ui/icons/Search";
import { fade, makeStyles } from "@material-ui/core/styles";
import { InputBase } from "@material-ui/core";

// Action
import { handleSearch } from "../../../state/data/actions";

const useStyles = makeStyles(theme => ({
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto"
    }
  },
  searchIcon: {
    width: theme.spacing(7),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit"
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: 120,
      "&:focus": {
        width: 200
      }
    }
  }
}));

const Search = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const {
    characters: { isLoading: isLoadingA },
    movies: { isLoading: isLoadingB },
    data: { search }
  } = useSelector(state => state);
  const [value, setValue] = useState("");

  useEffect(() => {
    if (search === "") setValue("");
  }, [search]);

  const handleSubmit = e => {
    e.preventDefault();
    dispatch(handleSearch(value));
  };

  const handleChange = ({ target }) => setValue(target.value);
  return (
    <form className={classes.search} onSubmit={handleSubmit}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        id="search"
        disabled={isLoadingA || isLoadingB}
        placeholder="Buscar..."
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput
        }}
        inputProps={{ "aria-label": "buscar" }}
        onChange={handleChange}
        value={value}
      />
    </form>
  );
};

export default Search;
