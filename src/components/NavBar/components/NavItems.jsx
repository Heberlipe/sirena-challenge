import React from "react";
import { Route } from "react-router-dom";

// Material
import {
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles
} from "@material-ui/core";

// Components
import Link from "../../Link/Link";

// Config
import { navItems } from "../../../config/routes";

const useStyles = makeStyles(theme => ({
  list: {
    padding: theme.spacing(0)
  }
}));

const NavItems = ({ onClick }) => {
  const classes = useStyles();

  return navItems.map(({ navList, key }) => (
    <React.Fragment key={key}>
      <Divider />
      <List className={classes.list}>
        {navList.map(({ Icon, text, to }) => (
          <Route key={to} path={to}>
            {({ match }) => (
              <ListItem
                selected={Boolean(match)}
                button
                onClick={onClick}
                component={Link}
                to={to}
              >
                {Icon && (
                  <ListItemIcon>
                    <Icon />
                  </ListItemIcon>
                )}
                <ListItemText primary={text} />
              </ListItem>
            )}
          </Route>
        ))}
      </List>
    </React.Fragment>
  ));
};

export default NavItems;
