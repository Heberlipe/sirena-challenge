import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import NavItems from "./NavItems";
import Logo from "../../Logo";

const useStyles = makeStyles(theme => ({
  toolbar: {
    ...theme.mixins.toolbar,
    alignItems: "center",
    display: "flex",
    justifyContent: "center"
  }
}));

const DrawerWrapper = ({ onClose }) => (
  <>
    <div className={useStyles().toolbar}>
      <Logo height="35px" />
    </div>
    <NavItems onClick={onClose} />
  </>
);

DrawerWrapper.propTypes = {
  onClose: PropTypes.func.isRequired
};

export default DrawerWrapper;
