import React from "react";
import PropTypes from "prop-types";
import { Drawer, Hidden, makeStyles, SwipeableDrawer } from "@material-ui/core";
import { iOS, drawerWidth } from "../../../config/config";
import { customPropTypes } from "../../../utils";

const useStyles = makeStyles(theme => ({
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0
    }
  },
  drawerPaper: {
    width: drawerWidth
  }
}));

const DrawerContainer = ({ mobileOpen, toggleDrawer, children }) => {
  const classes = useStyles();
  return (
    <nav className={classes.drawer}>
      <Hidden smUp>
        <SwipeableDrawer
          classes={{ paper: classes.drawerPaper }}
          open={mobileOpen}
          onClose={toggleDrawer(false)}
          onOpen={toggleDrawer(true)}
          disableBackdropTransition={!iOS}
          disableDiscovery={iOS}
        >
          {children}
        </SwipeableDrawer>
      </Hidden>
      <Hidden xsDown>
        <Drawer
          classes={{ paper: classes.drawerPaper }}
          variant="permanent"
          open
        >
          {children}
        </Drawer>
      </Hidden>
    </nav>
  );
};

DrawerContainer.propTypes = {
  mobileOpen: PropTypes.bool.isRequired,
  toggleDrawer: PropTypes.func.isRequired,
  children: customPropTypes.children.isRequired
};

export default DrawerContainer;
