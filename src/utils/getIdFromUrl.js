const getIdFromUrl = url => url.split("/")[5];

export default getIdFromUrl;
