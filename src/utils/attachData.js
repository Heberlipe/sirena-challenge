const attachData = (list, item, shouldAttach) =>
  item && shouldAttach ? [...list, item] : list;

export default attachData;
