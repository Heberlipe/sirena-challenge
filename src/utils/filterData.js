const filterData = (data, search) =>
  search
    ? data.filter(
        ({ name, title }) =>
          (name || title).toLowerCase().indexOf(search) !== -1
      )
    : data;

export default filterData;
