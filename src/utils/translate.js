import { yandex } from "../config/config";

const getUrl = text =>
  `${yandex.baseUrl}?key=${yandex.apiKey}&lang=en-es&text=${encodeURIComponent(
    text
  )}`;

const translate = text =>
  fetch(getUrl(text), { method: "POST" }).then(res => res.json());

export default translate;
