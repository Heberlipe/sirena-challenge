export const iOS =
  process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

// SWAPI
export const baseUrl = "https://swapi.dev/api";

// Yandex
export const yandex = {
  apiKey:
    "trnsl.1.1.20190902T222934Z.269e60371ad6d432.e21c445999c91f722b49cbaab43216174ec878c5",
  baseUrl: "https://translate.yandex.net/api/v1.5/tr.json/translate"
};

// Styles
export const drawerWidth = 240;
export const starWarsColor = "#FFC500";
