import { Redirect } from "react-router-dom";

// Icons
import VideocamIcon from "@material-ui/icons/VideocamOutlined";
import PersonIcon from "@material-ui/icons/PersonOutlined";
import InfoIcon from "@material-ui/icons/InfoOutlined";

// Components
import { ContentContainer, InfoContainer } from "../containers";

const routeConfig = {
  personajes: {
    Icon: PersonIcon,
    params: "/:id?",
    path: "/personajes",
    showSearch: true,
    title: "Personajes",
    route: {
      component: ContentContainer,
      props: {
        type: "characters"
      }
    }
  },
  peliculas: {
    Icon: VideocamIcon,
    params: "/:id?",
    path: "/peliculas",
    showSearch: true,
    title: "Películas",
    route: {
      component: ContentContainer,
      props: {
        type: "movies"
      }
    }
  },
  info: {
    Icon: InfoIcon,
    path: "/info",
    params: "",
    showSearch: false,
    title: "Información",
    route: {
      component: InfoContainer,
      props: {
        type: "info"
      }
    }
  },
  redirect: {
    route: {
      component: Redirect,
      props: {
        to: "/personajes"
      }
    }
  }
};

export const getNavProps = pathname => {
  const id = pathname.split("/")[1];
  const { title, showSearch } = routeConfig[id] || {};
  return { title, showSearch };
};

const getLinkData = id => {
  const { Icon, path, title } = routeConfig[id];
  return {
    Icon,
    text: title,
    to: path
  };
};

export const navItems = [
  {
    key: 1,
    navList: [getLinkData("personajes"), getLinkData("peliculas")]
  },
  {
    key: 2,
    navList: [getLinkData("info")]
  }
];

const routeList = Object.keys(routeConfig);

export const routes = routeList.map(id => {
  const { route, path, params } = routeConfig[id];
  return {
    ...route,
    path: id !== "redirect" ? `${path}${params}` : "",
    id
  };
});
