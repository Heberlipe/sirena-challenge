import { combineReducers } from "redux";
import { constant } from "lodash/fp";
import { createReducer } from "../../utils";

import { ON_SEARCH, RESET_SEARCH } from "./actionTypes";

export const search = createReducer("", {
  [ON_SEARCH]: (state, { payload }) => payload,
  [RESET_SEARCH]: constant("")
});

export default combineReducers({
  search
});
