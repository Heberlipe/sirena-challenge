import { fetchCharacters, selectCharacter } from "../characters/actions";
import { fetchMovies, selectMovie } from "../movies/actions";

import { ON_SEARCH, RESET_SEARCH } from "./actionTypes";

const actions = {
  characters: {
    select: selectCharacter,
    fetch: fetchCharacters
  },
  movies: {
    select: selectMovie,
    fetch: fetchMovies
  }
};

export const fetchData = type => dispatch => dispatch(actions[type].fetch());

export const selectItem = (type, payload) => dispatch =>
  dispatch(actions[type].select(payload));

export const handleSearch = payload => ({
  type: ON_SEARCH,
  payload: payload.toLowerCase()
});

export const resetSearch = () => ({
  type: RESET_SEARCH
});
