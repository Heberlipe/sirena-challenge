export const GET_DATA = "data/GET_DATA";
export const ON_SEARCH = "data/ON_SEARCH";
export const RESET_SEARCH = "data/RESET_SEARCH";
