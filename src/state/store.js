import { applyMiddleware, combineReducers, createStore, compose } from "redux";
import thunkMiddleware from "redux-thunk";
import characters from "./characters/reducer";
import movies from "./movies/reducer";
import detail from "./detail/reducer";
import data from "./data/reducer";

const rootReducer = combineReducers({
  characters,
  data,
  detail,
  movies
});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
  rootReducer,
  composeEnhancer(applyMiddleware(thunkMiddleware))
);
