import { combineReducers } from "redux";
import { constant } from "lodash/fp";
import { createReducer } from "../../utils";

import { OPEN_DETAIL, CLOSE_DETAIL } from "./actionTypes";

export const isOpen = createReducer(false, {
  [OPEN_DETAIL]: constant(true),
  [CLOSE_DETAIL]: constant(false)
});

export default combineReducers({
  isOpen
});
