import { OPEN_DETAIL, CLOSE_DETAIL } from "./actionTypes";

export const openDetail = () => ({
  type: OPEN_DETAIL
});

export const closeDetail = () => ({
  type: CLOSE_DETAIL
});
