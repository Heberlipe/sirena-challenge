import { combineReducers } from "redux";
import { constant } from "lodash/fp";
import { createReducer } from "../../utils";
import { baseUrl } from "../../config/config";

import {
  LOAD_CHARACTER_FAIL,
  LOAD_CHARACTER_SUCCESS,
  REQUEST_CHARACTERS_FAIL,
  REQUEST_CHARACTERS_START,
  REQUEST_CHARACTERS_SUCCESS,
  SEARCH_CHARACTERS_FAIL,
  SEARCH_CHARACTERS_START,
  SEARCH_CHARACTERS_SUCCESS,
  RESET_CHARACTERS,
  SELECT_CHARACTER,
  SET_EXISTENCE
} from "./actionTypes";

export const data = createReducer([], {
  [REQUEST_CHARACTERS_SUCCESS]: (state, { payload }) => [
    ...state,
    ...payload.characters
  ],
  [REQUEST_CHARACTERS_FAIL]: constant([]),
  [RESET_CHARACTERS]: constant([])
});

export const searchData = createReducer([], {
  [SEARCH_CHARACTERS_SUCCESS]: (state, { payload }) => payload,
  [SEARCH_CHARACTERS_FAIL]: constant([]),
  [SEARCH_CHARACTERS_START]: constant([])
});

export const selectedFromOutside = createReducer(null, {
  [LOAD_CHARACTER_SUCCESS]: (state, { payload }) => payload
});

export const selectedAlreadyExists = createReducer(false, {
  [SET_EXISTENCE]: (state, { payload }) => payload,
  [LOAD_CHARACTER_FAIL]: constant(null)
});

export const selected = createReducer(
  {},
  {
    [SELECT_CHARACTER]: (state, { payload }) => payload
  }
);

const initialPath = `${baseUrl}/people`;

export const path = createReducer(initialPath, {
  [REQUEST_CHARACTERS_SUCCESS]: (state, { payload }) => payload.next,
  [REQUEST_CHARACTERS_FAIL]: constant(initialPath),
  [RESET_CHARACTERS]: constant(initialPath)
});

export const isLoading = createReducer(false, {
  [REQUEST_CHARACTERS_START]: constant(true),
  [REQUEST_CHARACTERS_SUCCESS]: constant(false),
  [REQUEST_CHARACTERS_FAIL]: constant(false),
  [SEARCH_CHARACTERS_START]: constant(true),
  [SEARCH_CHARACTERS_SUCCESS]: constant(false),
  [SEARCH_CHARACTERS_FAIL]: constant(false),
  [LOAD_CHARACTER_SUCCESS]: constant(false)
});

export const error = createReducer(null, {
  [REQUEST_CHARACTERS_START]: constant(null),
  [REQUEST_CHARACTERS_SUCCESS]: constant(null),
  [REQUEST_CHARACTERS_FAIL]: (state, { payload }) => payload
});

export default combineReducers({
  data,
  error,
  isLoading,
  path,
  selected,
  selectedFromOutside,
  selectedAlreadyExists,
  searchData
});
