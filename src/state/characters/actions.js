import {
  LOAD_CHARACTER_FAIL,
  LOAD_CHARACTER_SUCCESS,
  REQUEST_CHARACTERS_FAIL,
  REQUEST_CHARACTERS_START,
  REQUEST_CHARACTERS_SUCCESS,
  SEARCH_CHARACTERS_FAIL,
  SEARCH_CHARACTERS_START,
  SEARCH_CHARACTERS_SUCCESS,
  RESET_CHARACTERS,
  SELECT_CHARACTER,
  SET_EXISTENCE
} from "./actionTypes";
import { baseUrl } from "../../config/config";
import { getIdFromUrl } from "../../utils";

export const selectCharacter = payload => ({
  type: SELECT_CHARACTER,
  payload
});

export const resetCharacters = () => ({
  type: RESET_CHARACTERS
});

export const getCharacters = () => ({
  type: REQUEST_CHARACTERS_START
});

export const getCharactersSuccess = (payload, replace) => ({
  type: REQUEST_CHARACTERS_SUCCESS,
  payload,
  replace
});

export const getCharactersFail = payload => ({
  type: REQUEST_CHARACTERS_FAIL,
  payload
});

export const setExistence = payload => ({
  type: SET_EXISTENCE,
  payload
});

export const fetchCharacters = () => async (dispatch, getState) => {
  const { path, selectedFromOutside } = getState().characters;

  dispatch(getCharacters());

  try {
    const response = await fetch(path);
    const { results, next } = await response.json();

    const characters = results.map(character => {
      const id = getIdFromUrl(character.url);
      if (selectedFromOutside && id === selectedFromOutside.id) {
        dispatch(setExistence(true));
      }
      return {
        ...character,
        id,
        films: character.films.map(film => getIdFromUrl(film))
      };
    });

    return dispatch(
      getCharactersSuccess({
        characters,
        next
      })
    );
  } catch (error) {
    return dispatch(getCharactersFail(error));
  }
};

export const loadCharacterFail = payload => ({
  type: LOAD_CHARACTER_FAIL,
  payload
});

export const loadCharacterSuccess = (payload, replace) => ({
  type: LOAD_CHARACTER_SUCCESS,
  payload,
  replace
});

export const loadCharacter = id => async dispatch => {
  const url = `${baseUrl}/people/${id}`;
  dispatch(getCharacters());

  try {
    const response = await fetch(url);
    const character = await response.json();

    return dispatch(
      loadCharacterSuccess({
        ...character,
        id: getIdFromUrl(character.url),
        films: character.films.map(film => getIdFromUrl(film))
      })
    );
  } catch (error) {
    return dispatch(loadCharacterFail(error));
  }
};

export const searchCharacters = () => ({
  type: SEARCH_CHARACTERS_START
});

export const searchCharactersSuccess = (payload, replace) => ({
  type: SEARCH_CHARACTERS_SUCCESS,
  payload,
  replace
});

export const searchCharactersFail = payload => ({
  type: SEARCH_CHARACTERS_FAIL,
  payload
});

export const fetchSearchCharacters = search => async dispatch => {
  const url = `${baseUrl}/people/?search=${search}`;

  if (search !== "") {
    dispatch(searchCharacters());

    try {
      const response = await fetch(url);
      const { results } = await response.json();

      return dispatch(
        searchCharactersSuccess(
          results.map(character => ({
            ...character,
            id: getIdFromUrl(character.url),
            films: character.films.map(film => getIdFromUrl(film))
          }))
        )
      );
    } catch (error) {
      return dispatch(searchCharactersFail(error));
    }
  } else {
    return dispatch(searchCharactersSuccess({}));
  }
};
