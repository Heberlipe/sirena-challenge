import {
  REQUEST_MOVIES_START,
  REQUEST_MOVIES_SUCCESS,
  REQUEST_MOVIES_FAIL,
  SELECT_MOVIE
} from "./actionTypes";
import { getIdFromUrl } from "../../utils";
import { baseUrl } from "../../config/config";

export const selectMovie = payload => ({
  type: SELECT_MOVIE,
  payload
});

export const getMovies = () => ({
  type: REQUEST_MOVIES_START
});

export const getMoviesSuccess = payload => ({
  type: REQUEST_MOVIES_SUCCESS,
  payload
});

export const getMoviesFail = payload => ({
  type: REQUEST_MOVIES_FAIL,
  payload
});

const url = `${baseUrl}/films`;
export const fetchMovies = () => async dispatch => {
  dispatch(getMovies());
  try {
    const response = await fetch(url);
    const { results } = await response.json();
    return dispatch(
      getMoviesSuccess(
        results.map(item => ({
          ...item,
          id: getIdFromUrl(item.url)
        }))
      )
    );
  } catch (error) {
    return dispatch(getMoviesFail(error));
  }
};
