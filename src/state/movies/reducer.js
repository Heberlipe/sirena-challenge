import { combineReducers } from "redux";
import { constant } from "lodash/fp";
import { createReducer } from "../../utils";

import {
  REQUEST_MOVIES_START,
  REQUEST_MOVIES_SUCCESS,
  REQUEST_MOVIES_FAIL,
  SELECT_MOVIE
} from "./actionTypes";

export const data = createReducer([], {
  [REQUEST_MOVIES_SUCCESS]: (state, { payload }) => payload,
  [REQUEST_MOVIES_FAIL]: constant([])
});

export const selected = createReducer(
  {},
  {
    [SELECT_MOVIE]: (state, { payload }) => payload
  }
);

export const isLoading = createReducer(false, {
  [REQUEST_MOVIES_START]: constant(true),
  [REQUEST_MOVIES_SUCCESS]: constant(false),
  [REQUEST_MOVIES_FAIL]: constant(false)
});

export const error = createReducer(null, {
  [REQUEST_MOVIES_START]: constant(null),
  [REQUEST_MOVIES_SUCCESS]: constant(null),
  [REQUEST_MOVIES_FAIL]: (state, { payload }) => payload
});

export default combineReducers({
  data,
  error,
  isLoading,
  selected
});
