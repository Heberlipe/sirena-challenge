/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { NavBar } from "../components";
import { routes } from "../config/routes";
import SectionContainer from "../containers/SectionContainer";

const RouteHandler = route => (
  <Route
    exact={route.isExact}
    path={route.path}
    render={props => (
      <SectionContainer>
        <route.component {...props} {...route.props} routes={route.routes} />
      </SectionContainer>
    )}
  />
);

const Routes = () => (
  <Router>
    <NavBar />
    <Switch>
      {routes.map(route => (
        <RouteHandler key={route.id} {...route} />
      ))}
    </Switch>
  </Router>
);

export default Routes;
