import React, { useEffect } from "react";

// Material
import { Provider, useDispatch } from "react-redux";
import { ThemeProvider } from "@material-ui/styles";
import { createMuiTheme, CssBaseline } from "@material-ui/core";

import { fetchData } from "./state/data/actions";
import { starWarsColor } from "./config/config";
import Router from "./router/Router";
import store from "./state/store";

const theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: starWarsColor
    }
  }
});

const Content = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchData("movies"));
  }, [dispatch]);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router />
    </ThemeProvider>
  );
};

const App = () => (
  <Provider store={store}>
    <Content />
  </Provider>
);

export default App;
