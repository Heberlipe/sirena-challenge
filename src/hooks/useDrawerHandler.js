import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { openDetail, closeDetail } from "../state/detail/actions";
import { selectItem } from "../state/data/actions";

const useDrawerHandler = ({ type, id }) => {
  const dispatch = useDispatch();
  const { data, selectedFromOutside, selectedAlreadyExists } = useSelector(
    state => state[type]
  );

  useEffect(() => {
    if (id) {
      const list =
        selectedFromOutside && !selectedAlreadyExists
          ? [...data, selectedFromOutside]
          : data;
      const selectedItem = list.find(item => item.id === id);
      if (selectedItem) {
        dispatch(openDetail());
        dispatch(selectItem(type, selectedItem));
      }
    } else {
      dispatch(closeDetail());
    }
    return () => dispatch(selectItem(type, {}));
  }, [id, data, dispatch, type, selectedFromOutside, selectedAlreadyExists]);
};

export default useDrawerHandler;
