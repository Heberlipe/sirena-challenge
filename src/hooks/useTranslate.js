import { useState, useEffect } from "react";
import { isEmpty } from "lodash/fp";
import { translate } from "../utils";

const useTranslate = (data, key) => {
  const [isLoading, setLoading] = useState(false);
  const [content, setContent] = useState({});

  useEffect(() => {
    if (!isEmpty(data)) {
      setLoading(true);
      translate(data[key])
        .then(({ text }) => {
          setContent({
            ...data,
            [key]: text.join(", ")
          });
          setLoading(false);
        })
        .catch(() => setLoading(false));
    }
    return () => setLoading(true);
  }, [data, key]);

  return { content, isLoading, isEmpty: isEmpty(content) };
};

export default useTranslate;
