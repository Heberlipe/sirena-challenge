import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { drawerWidth } from "../../config/config";
import { customPropTypes } from "../../utils";

const useStyles = makeStyles(theme => ({
  content: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    [theme.breakpoints.up("sm")]: {
      paddingLeft: drawerWidth
    }
  },
  topSpace: theme.mixins.toolbar
}));

const SectionContainer = ({ children }) => {
  const classes = useStyles();
  return (
    <main className={classes.content}>
      <div className={classes.topSpace} />
      {children}
    </main>
  );
};

SectionContainer.propTypes = {
  children: customPropTypes.children
};

SectionContainer.defaultProps = {
  children: null
};

export default SectionContainer;
