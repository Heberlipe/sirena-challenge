import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import { makeStyles, Typography, Paper } from "@material-ui/core";

// Components
import { ContentList } from "../../../components";
import LoadMoreHandler from "./LoadMoreHandler";

// Utils
import { filterData, attachData } from "../../../utils/index";
import { fetchData, resetSearch } from "../../../state/data/actions";
import { fetchSearchCharacters } from "../../../state/characters/actions";

const useStyles = makeStyles(theme => ({
  paper: {
    overflow: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: "100%"
  }
}));

const ContentHandler = ({ handleSelect, selected, type }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const {
    [type]: {
      data,
      error,
      isLoading,
      selectedFromOutside,
      selectedAlreadyExists,
      searchData
    },
    data: { search }
  } = useSelector(state => state);

  useEffect(() => () => dispatch(resetSearch()), [type, dispatch]);

  useEffect(() => {
    if (type === "characters") dispatch(fetchSearchCharacters(search));
  }, [type, dispatch, search]);

  useEffect(() => {
    if (type === "characters" && data.length === 0 && !isLoading && !search) {
      dispatch(fetchData(type));
    }
  }, [dispatch, data, type, isLoading, search]);

  const renderData =
    type === "characters" && searchData.length === 0
      ? searchData
      : filterData(
          attachData(data, selectedFromOutside, !selectedAlreadyExists),
          search
        );

  return (
    <Paper className={classes.paper}>
      {error ? (
        <Typography variant="caption" paragraph align="center">
          Lo sentimos, ocurrió un error al procesar la solicitud. Por favor,
          intente nuevamente mas tarde.
        </Typography>
      ) : (
        <ContentList
          data={renderData}
          isLoading={isLoading}
          onClick={handleSelect}
          selected={selected}
          search={search}
        />
      )}
      <LoadMoreHandler type={type} />
    </Paper>
  );
};

ContentHandler.propTypes = {
  handleSelect: PropTypes.func.isRequired,
  selected: PropTypes.string,
  type: PropTypes.string.isRequired
};

ContentHandler.defaultProps = {
  selected: ""
};

export default ContentHandler;
