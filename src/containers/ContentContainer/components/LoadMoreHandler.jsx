import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { isEmpty } from "lodash/fp";
import { useSelector, useDispatch } from "react-redux";
import { fetchData } from "../../../state/data/actions";
import { LoadMore } from "../../../components";

const LoadMoreHandler = ({ type }) => {
  const { isLoading, path, error, searchData, data } = useSelector(
    state => state[type]
  );
  const dispatch = useDispatch();
  const [shouldLoadMore, setShouldLoadMore] = useState(true);

  useEffect(() => {
    if (!isLoading) setShouldLoadMore(true);
  }, [isLoading]);

  const handleLoadMore = () => {
    if (shouldLoadMore && path) {
      setShouldLoadMore(false);
      dispatch(fetchData(type));
    }
  };

  const shouldRender = {
    characters: path && !error && (!searchData || isEmpty(searchData)),
    movies: data.length === 0
  };

  return shouldRender[type] ? (
    <LoadMore onClick={handleLoadMore} isLoading={isLoading} />
  ) : null;
};

LoadMoreHandler.propTypes = {
  type: PropTypes.string.isRequired
};

export default LoadMoreHandler;
