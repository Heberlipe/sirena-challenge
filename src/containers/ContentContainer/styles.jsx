import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    margin: theme.spacing(2, 0),
    alignItems: "flex-start",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  center: {
    display: "flex",
    justifyContent: "center",
    padding: theme.spacing(2, 0)
  },
  contentShift: {
    [theme.breakpoints.up("sm")]: {
      width: "50%"
    }
  }
}));

export default useStyles;
