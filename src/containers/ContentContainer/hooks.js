import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { loadCharacter } from "../../state/characters/actions";

const useShareListener = ({ type, id }) => {
  const { data } = useSelector(state => state[type]);
  const dispatch = useDispatch();

  useEffect(() => {
    if (
      type === "characters" &&
      id &&
      data.length > 0 &&
      !data.find(item => item.id === id)
    ) {
      dispatch(loadCharacter(id));
    }
  }, [type, dispatch, data, id]);
};

export default useShareListener;
