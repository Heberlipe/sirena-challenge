import React from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import clsx from "clsx";

// Components
import { DetailDrawer, SectionWrapper } from "../../components";

// Utils
import useStyles from "./styles";
import ContentHandler from "./components/ContentHandler";
import { useDrawerHandler, useShareListener } from "../../hooks";

const baseUrl = {
  characters: "personajes",
  movies: "peliculas"
};

const ContentContainer = ({
  history: { replace },
  match: {
    params: { id }
  },
  type
}) => {
  const classes = useStyles();
  const {
    [type]: { selected },
    detail: { isOpen }
  } = useSelector(state => state);

  useShareListener({ id, type });
  useDrawerHandler({ id, type });

  const handleSelect = (to = "") => () => replace(`/${baseUrl[type]}/${to}`);

  return (
    <SectionWrapper
      className={clsx(classes.root, {
        [classes.contentShift]: isOpen
      })}
    >
      <ContentHandler handleSelect={handleSelect} type={type} selected={id} />
      <DetailDrawer
        data={selected}
        onClose={handleSelect}
        type={type}
        id={id}
      />
    </SectionWrapper>
  );
};

ContentContainer.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    })
  }).isRequired,
  type: PropTypes.string.isRequired
};

export default ContentContainer;
