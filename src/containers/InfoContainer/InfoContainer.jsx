import React from "react";
import {
  Link,
  List,
  ListItem,
  makeStyles,
  Paper,
  Typography
} from "@material-ui/core";
import { SectionWrapper } from "../../components";
import { starWarsColor } from "../../config/config";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  },
  title: {
    width: "100%"
  },
  link: {
    color: starWarsColor
  }
}));

const repoUrl = "https://bitbucket.org/Heberlipe/sirena-challenge/src/master/";
const apiUrl = "https://swapi.dev/";
const yandexUrl = "https://tech.yandex.com/translate/";

const InfoContainer = () => {
  const classes = useStyles();
  return (
    <SectionWrapper>
      <Paper className={classes.root}>
        <Typography align="center" className={classes.title} variant="h4">
          Star Wars
        </Typography>
        <Typography align="center" className={classes.title} variant="h5">
          The Code Challenge
        </Typography>
        <Typography align="center" gutterBottom className={classes.title}>
          <Typography
            align="center"
            gutterBottom
            className={classes.title}
            variant="caption"
          >
            by Heber Acosta Lipera
          </Typography>
        </Typography>
        <Typography gutterBottom variant="h6">
          Caracteristicas
        </Typography>
        <Typography paragraph gutterBottom variant="body2">
          {`Esta aplicacion es un examen tecnico de desarrollo web. Consume informacion desde una `}
          <Link className={classes.link} href={apiUrl}>
            API de Star Wars
          </Link>
          {` para mostrar los personajes y peliculas de la franquicia en sus respectivas pestañas.`}
        </Typography>
        <Typography paragraph gutterBottom variant="body2">
          Los components visuales son 100% responsive.
        </Typography>
        <Typography paragraph gutterBottom variant="body2">
          Al seleccionar o ingresar mediante url a un elemento, ya sea personaje
          o pelicula, se puede acceder al detalle del mismo en el menu lateral.
        </Typography>
        <Typography paragraph gutterBottom variant="body2">
          {`Los textos en español de los detalles se obtienen por medio de la API de traducciones de `}
          <Link className={classes.link} href={yandexUrl}>
            Yandex
          </Link>
          .
        </Typography>
        <Typography paragraph gutterBottom variant="body2">
          La busqueda de los personajes se realiza directamente contra el
          servicio proveedor del contenido. La busqueda de peliculas funciona a
          modo de filtro segun los caracteres introducidos.
        </Typography>
        <Typography paragraph gutterBottom variant="body2">
          {`El repositorio junto con el codigo fuente puede ser encontrado en `}
          <Link className={classes.link} href={repoUrl}>
            Bitbucket
          </Link>
          .
        </Typography>
        <Typography variant="h6">Frameworks</Typography>
        <List>
          <ListItem>
            <Typography variant="body2">React (Hooks)</Typography>
          </ListItem>
          <ListItem>
            <Typography variant="body2">React Router</Typography>
          </ListItem>
          <ListItem>
            <Typography variant="body2">Redux (Thunk)</Typography>
          </ListItem>
          <ListItem>
            <Typography variant="body2">Material UI</Typography>
          </ListItem>
          <ListItem>
            <Typography variant="body2">Firebase Hosting</Typography>
          </ListItem>
        </List>
      </Paper>
    </SectionWrapper>
  );
};

export default InfoContainer;
