# Star Wars: The Code Challenge

## Informacion

Esta aplicacion es un examen tecnico de desarrollo web.

[Ambiente de produccion](https://sirena-challenge.web.app).

Este proyecto fue creado via [Create React App](https://github.com/facebook/create-react-app).

## Inicializar

- Tener instalado [Node.js](https://nodejs.org/es/)
- Ejecutar `npm install` en el directorio del raiz

## Compilar

- Ejecutar `npm start`
- Abrir [localhost:3000](localhost:3000) desde el navegador

## Tests

- Ejecutar `npm run test` en el directorio raiz

## CI-CD

El branch `master` de este proyecto se encuentra sincronizado con Codeship. Todos los cambios realizados en este branch se despliegan en el [ambiente de produccion](https://sirena-challenge.web.app).
